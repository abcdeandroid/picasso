package com.abcdeandroid.picasso;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.GridView;

public class MainActivity extends ActionBarActivity {

	private GridView gridView;
	private GridViewAdapter gridViewAdapter;
	
	private ArrayList <String> imagenes = new ArrayList<String>();
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		imagenes.add("http://milimagenescristianas.com/wp-content/uploads/2014/11/imagenes-cristianas-gratis.jpg");
		imagenes.add("http://milimagenescristianas.com/wp-content/uploads/2014/11/imagenes-cristianas-gratis.jpg");
		imagenes.add("http://milimagenescristianas.com/wp-content/uploads/2014/11/imagenes-cristianas-gratis.jpg");
		imagenes.add("http://milimagenescristianas.com/wp-content/uploads/2014/11/imagenes-cristianas-gratis.jpg");

		gridView = (GridView)findViewById(R.id.mainGridView);
		gridViewAdapter = new GridViewAdapter(getApplicationContext(), imagenes);
		
		gridView.setAdapter(gridViewAdapter);
	}

}
