package com.abcdeandroid.picasso;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class GridViewAdapter extends BaseAdapter {
	
	private final Context contexto;
	private final ArrayList <String> imagenes;
	
	public GridViewAdapter (Context contexto, ArrayList <String> imagenes){
		this.contexto = contexto;
		this.imagenes = imagenes;
	}
	

	@Override
	public int getCount() {
		return imagenes.size();
	}

	@Override
	public Object getItem(int posicion) {
		return imagenes.get(posicion);
	}

	@Override
	public long getItemId(int posicion) {
		return posicion;
	}

	@Override
	public View getView(int posicion, View convertView, ViewGroup parent) {
		ImageView imageView = null;
		if (convertView == null){
			imageView = new ImageView (contexto);
			convertView = imageView; 
			imageView.setPadding(8, 8, 8, 8);
			
		}else{
			imageView = (ImageView)convertView;
			
		}
		Picasso.with(contexto).load("http://milimagenescristianas.com/wp-content/uploads/2014/11/imagenes-cristianas-gratis.jpg").into (imageView);
		return convertView;
		
	}

}
